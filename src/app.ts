import createError from "http-errors";
import express, { NextFunction, Request, Response } from "express";
import path, { join } from "path";
import cookieParser from "cookie-parser";
import logger from "morgan";
import helmet from "helmet";
import cors from "cors";
import chalk from "chalk";

import indexRouter from "./routes/index";
import { NODE_ENV } from "./configs/varriable.configs";
import { connection } from "./configs/database.config";
import { SequelizeScopeError } from "sequelize/types";

const rfs = require("rotating-file-stream");
const isProduction = NODE_ENV === "production";
class App {
  public express: express.Express;
  constructor() {
    this.express = express();
    connection
      .authenticate()
      .then(() =>
        console.log(chalk.blue("Connection has been established successfully."))
      )
      .catch((err: SequelizeScopeError) => {
        console.error(chalk.red("Unable to connect to the database:", err));
      });
    this.express.use(helmet());
    this.express.use(cors());

    // view engine setup
    this.express.set("views", path.join(__dirname, "views"));
    this.express.set("view engine", "pug");

    // Create logs
    const accessLogStream = rfs.createStream("access.log", {
      interval: "1d",
      path: join(__dirname, "../logs"),
    });

    this.express.use(
      isProduction
        ? logger("combined", { stream: accessLogStream })
        : logger("dev")
    );
    this.express.use(express.json());
    this.express.use(express.urlencoded({ extended: false }));
    this.express.use(cookieParser());
    this.express.use(express.static(path.join(__dirname, "public")));
    this.loadRoutes();
    this.handleError();
  }
  private loadRoutes(): void {
    new indexRouter(this.express);
  }
  private handleError(): void {
    // catch 404 and forward to error handler
    this.express.use((req, res, next) => {
      next(createError(404));
    });

    // error handler
    this.express.use(function (
      err: any,
      req: Request,
      res: Response,
      next: NextFunction
    ) {
      // set locals, only providing error in development
      res.locals.message = err.message;
      res.locals.error = req.app.get("env") === "development" ? err : {};

      // console.log('local error');
      // console.log(res.locals);

      // render the error page
      res.status(err.status || 500);
      res.json(res.locals);
    });
  }
}

export default App;
