import express from "express";
import { orderController } from "../controllers/order.controllers";

const router = express.Router();

router
  .route("/api/v1/orders")
  .get(orderController.getAll)
  .post(orderController.create);
router
  .route("/api/v1/orders/:id")
  .get(orderController.getOne)
  .patch(orderController.update);

export { router as orderRoute };
