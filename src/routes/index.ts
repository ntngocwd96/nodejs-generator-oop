import express, { Request, Response } from "express";
import { orderRoute } from "./order.routes";

class Router {
  constructor(app: express.Express) {
    /* GET home page. */
    const router = express.Router();
    router.get("/", function (req: Request, res: Response) {
      res.render("index", { title: "Express" });
    });
    router.use(orderRoute)
    app.use("/", router);
  }
}

export default Router;
