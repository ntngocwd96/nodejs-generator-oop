import httpStatus from 'http-status';

class ApplicationError extends Error {
  status: string;
  errors: {};
  isOperational: boolean;
  constructor(message: string, status: string, errors: {}, stack: string) {
    super(message);

    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.status = status;
    this.stack = stack;
    this.errors = errors;
    this.isOperational = true;
  }
}

/**
 * Class representing an API error
 * @extends ApplicationError
 */

class APIError extends ApplicationError {
  /**
   * Creates an API error.
   * @param {String} message - Error message
   * @param {Number} status - HTTP status code of error
   */
  constructor(
    message = 'Opps, something went wrong...',
    status = httpStatus.INTERNAL_SERVER_ERROR,
    errors: {},
    stack: string,
  ) {
    super(message, status.toString(), errors, stack);
  }
}

export { APIError };