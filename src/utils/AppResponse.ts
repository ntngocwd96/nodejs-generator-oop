import { Response } from "express";

class AppResponse{
  constructor() {
  }

  public static responseTemplate(res: Response,code: number, message: string, content: {} | null) {
    res.status(code).json({
      code,
      message,
      content
    })
  }
  public static responseNoContentTemplate(res: Response,code: number, message: string) {
    res.status(code).json({
      code,
      message,
    })
  }
}

export { AppResponse };