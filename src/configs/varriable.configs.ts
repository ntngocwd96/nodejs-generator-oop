import { config } from 'dotenv';
import { config as configSafe } from 'dotenv-safe';

config();
configSafe();

const { NODE_ENV, PORT } = process.env;

export { NODE_ENV, PORT };
