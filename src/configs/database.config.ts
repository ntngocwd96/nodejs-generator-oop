import { Sequelize } from 'sequelize';

// Option 1: Passing parameters separately
export const connection = new Sequelize(
  'sale-ecommerce',
  'usernameall',
  'root',
  {
    host: '172.21.0.5',
    dialect: 'mysql' /* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  },
);