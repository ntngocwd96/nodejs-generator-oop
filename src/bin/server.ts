/**
 * Module dependencies.
 */

import http from "http";
import chalk from "chalk";
const debug = require("debug")("auto-checking-payment:server");

import { PORT } from "../configs/varriable.configs";
import App from "../app";

class Server {
  private httpServer: any;
  private port: any;
  constructor() {
    this.httpServer = http.createServer(new App().express);
    /**
     * Get port from environment and store in Express.
     */
    this.port = this.normalizePort(PORT || "3000");
    // console.log(this.httpServer)
    this.httpServer.on("error", this.onError);
  }
  /**
   * Normalize a port into a number, string, or false.
   */
  /**
   * Listen
   */
  public Listen() {
    /**
     * Listen on provided port, on all network interfaces.
     */

    this.httpServer.listen(this.port, () =>
      console.log(chalk.bold.green(`Server listen on port: ${this.port}!!!`))
    );
  }

  private normalizePort(val: string) {
    const port = parseInt(val, 10);

    if (isNaN(port)) {
      // named pipe
      return val;
    }

    if (port >= 0) {
      // port number
      return port;
    }

    return false;
  }

  /**
   * Event listener for HTTP server "error" event.
   */

  private onError(error: NodeJS.ErrnoException) {
    if (error.syscall !== "listen") {
      throw error;
    }

    const bind =
      typeof this.port === "string" ? "Pipe " + this.port : "Port " + this.port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
      case "EACCES":
        console.error(bind + " requires elevated privileges");
        process.exit(1);
        break;
      case "EADDRINUSE":
        console.error(bind + " is already in use");
        process.exit(1);
        break;
      default:
        throw error;
    }
  }

}

const server = new Server();
server.Listen();

export default server;