// import httpStatus from "http-status";
// import { Request, Response, NextFunction } from "express";

import { BaseController } from "./base.controllers";
import { OrderService } from "../services/order.services";
// import { AppResponse } from "../utils/AppResponse";
// import { APIError } from "../utils/AppError";

class OrderController extends BaseController {
  constructor(public name: string, public key: string) {
    super(OrderService, name, key);
  }
}

const orderController = new OrderController("order", "productId");

export { orderController };
