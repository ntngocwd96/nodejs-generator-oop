import httpStatus from "http-status";
import { Request, Response, NextFunction } from "express";
import { APIError } from "../utils/AppError";

class BaseController {
  private Service: any;
  constructor(service: any, public name: string, public key: string) {
    this.Service = new service();
    console.log(service);

    this.getAll = this.getAll.bind(this);
    this.getOne = this.getOne.bind(this);
    this.update = this.update.bind(this);
    this.create = this.create.bind(this);
    this.delete = this.delete.bind(this);
    this.handleControllerError = this.handleControllerError.bind(this);
  }

  /**
   * getAll
   */
  public async getAll(req: Request, res: Response, next: NextFunction) {
    try {
      const offset = req.query.offset ?? 0;
      const limit = req.query.limit ?? 20;
      const instances = await this.Service.getAllService({
        offset: +offset,
        limit: +limit,
      });
      this.sendResponseWithContent(
        res,
        httpStatus.OK,
        `Get all ${this.name} successfully`,
        instances
      );
    } catch (error) {
      this.handleControllerError(error, next);
    }
  }

  /**
   * getOne
   */
  public async getOne(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const instance = await this.Service.getOneService({
        id,
        name: this.name,
        key: this.key,
      });
      this.sendResponseWithContent(
        res,
        httpStatus.OK,
        `Get specify ${this.name} successfully`,
        instance
      );
    } catch (error) {
      this.handleControllerError(error, next);
    }
  }

  /**
   * create
   */
  public async create(req: Request, res: Response, next: NextFunction) {
    try {
      const { body } = req;
      const instance = await this.Service.createService({ ...body });
      this.sendResponseWithContent(
        res,
        httpStatus.OK,
        `Create ${this.name} successfully!`,
        instance
      );
      throw new Error();
    } catch (error) {
      this.handleControllerError(error, next);
    }
  }

  /**
   * update
   */
  public async update(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const { body } = req;
      const rowAffected = await this.Service.updateService(
        { ...body },
        { key: this.key, id, name: this.name }
      );
      this.sendResponseWithNoContent(
        res,
        httpStatus.OK,
        `Update ${this.name} successfully! ${rowAffected} row(s) affected.`
      );
    } catch (error) {
      this.handleControllerError(error, next);
    }
  }

  /**
   * delete
   */
  public async delete(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const rowAffected = await this.Service.deleteService(
        { key: this.key, id, name: this.name }
      );
      this.sendResponseWithNoContent(
        res,
        httpStatus.OK,
        `Delete ${this.name} successfully! ${rowAffected} row(s) affected.`
      );
    } catch (error) {
      this.handleControllerError(error, next);
    }
  }

  public sendResponseWithContent(
    res: Response,
    code: number,
    message: string,
    content: {} | null
  ) {
    res.status(code).json({
      code,
      message,
      content,
    });
  }

  public sendResponseWithNoContent(
    res: Response,
    code: number,
    message: string
  ) {
    res.status(code).json({
      code,
      message,
    });
  }

  public handleControllerError(error: Error, next: NextFunction) {
    console.log("cagching error");
    if (error instanceof APIError) {
      next(error);
    } else {
      next(
        new APIError(
          "System error",
          httpStatus.INTERNAL_SERVER_ERROR,
          error,
          error.stack
        )
      );
    }
  }
}

export { BaseController };
