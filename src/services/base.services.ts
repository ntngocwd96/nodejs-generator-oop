import httpStatus from "http-status";
import { v4 as uuidv4 } from "uuid";

import { APIError } from "../utils/AppError";

class BaseService {
  private Model: any;
  constructor(Model: any) {
    this.Model = Model;
  }

  /**
   * getAll
   */
  public async getAllService(attr: { offset: number; limit: number }) {
    const { offset, limit } = attr;
    return await this.Model.findAll({
      offset,
      limit,
      where: {
        isDeleted: false,
      },
    });
  }

  /**
   * getOne
   */
  public async getOneService(attr: { key: string; id: string; name: string }) {
    const { key, id, name } = attr;
    const instance = await this.Model.findOne({
      where: {
        [key]: id,
        isDeleted: false,
      },
    });
    if (!instance) {
      throw new APIError(
        `No ${name} found or the ${name} was deleted!`,
        httpStatus.BAD_REQUEST,
        { message: `No ${name} found or the ${name} was deleted!` },
        ""
      );
    }
    return instance;
  }

  /**
   * Create
   */
  public async createService(attr: {}, obj: { key: string; name: string }) {
    const { key, name } = obj;
    const instance = await this.Model.create({
      ...attr,
      [key]: uuidv4(),
    });

    if (!instance) {
      throw new APIError(
        `Something get wrong when create ${name}! Please try again.`,
        httpStatus.BAD_REQUEST,
        { message: `Something get wrong when create! Please try again.` },
        ""
      );
    }
    return instance;
  }

  /**
   * update
   */
  public async updateService(
    attr: {},
    obj: { key: string; id: string; name: string }
  ) {
    const { key, id, name } = obj;
    const rowAffected = await this.Model.update(
      {
        ...attr,
      },
      {
        where: {
          [key]: id,
        },
      }
    );

    if (!rowAffected[0]) {
      throw new APIError(
        `Failure to update ${name}`,
        httpStatus.BAD_REQUEST,
        { message: `Failure to update ${name}` },
        ""
      );
    }
    return rowAffected[0];
  }

  /**
   * delete
   */
  public async deleteService(key: string, id: string, name: string) {
    const rowAffected = await this.Model.destroy({
      where: {
        [key]: id,
      },
    });
    if (!rowAffected) {
      throw new APIError(
        `Failure to delete ${name}!`,
        httpStatus.BAD_REQUEST,
        { message: `Failure to delete or no ${name} found` },
        ""
      );
    }
    return rowAffected;
  }
}

export { BaseService };
