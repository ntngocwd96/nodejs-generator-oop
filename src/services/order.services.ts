import { ProductModel } from "../models";
import { BaseService } from "./base.services";
class OrderService extends BaseService {
  constructor() {
    super(ProductModel);
  }
}

export { OrderService };
