FROM node:12-alpine

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3001

RUN ["chmod", "+x", "/usr/src/app/wait-for.sh"]

#CMD npm run watch
